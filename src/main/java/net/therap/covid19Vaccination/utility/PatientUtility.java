package net.therap.covid19Vaccination.utility;

import net.therap.covid19Vaccination.dao.*;
import net.therap.covid19Vaccination.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * @author aunabil.chakma
 * @since 01/04/2021
 **/
@Component
public class PatientUtility {

    private static final String REGISTER = "register";
    private static final String UPDATE = "update";
    private static final int NUMBER_OF_HOSPITAL = 2;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private BloodGroupDao bloodGroupDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private InstitutionDao institutionDao;

    @Autowired
    private StreetDao streetDao;

    @Autowired
    private ThanaDao thanaDao;

    public void setupModel(Patient patient, Model model) {
        String action = (patient.isNew()) ? REGISTER : UPDATE;

        model.addAttribute("patient", patient);
        model.addAttribute("action", action);
        model.addAttribute("bloodGroupList", bloodGroupDao.findAll());
        model.addAttribute("categoryList", categoryDao.findAll());
        model.addAttribute("districtList", districtDao.findAll());
        model.addAttribute("institutionList", institutionDao.findAll());
        model.addAttribute("streetList", streetDao.findAll());
        model.addAttribute("thanaList", thanaDao.findAll());
        model.addAttribute("numberOfHospital", NUMBER_OF_HOSPITAL);
    }

    public void setupQueryModel(Model model) {
        model.addAttribute("bloodGroupList", bloodGroupDao.findAll());
        model.addAttribute("categoryList", categoryDao.findAll());
        model.addAttribute("institutionList", institutionDao.findAll());
        model.addAttribute("districtList", districtDao.findAll());
        model.addAttribute("thanaList", thanaDao.findAll());
        model.addAttribute("streetList", streetDao.findAll());
    }

    public PatientQuery parseQuery(HttpServletRequest request) {
        PatientQuery query = new PatientQuery();

        String fromDateString = (String) request.getParameter("fromDate");
        if (fromDateString.length() > 0) {
            query.setFromDate(LocalDate.parse(fromDateString));
        }

        String toDateString = (String) request.getParameter("toDate");
        if (toDateString.length() > 0) {
            query.setToDate(LocalDate.parse(toDateString));
        }

        int categoryId = Integer.parseInt((String) request.getParameter("category"));
        query.setCategory(categoryDao.findById(categoryId));

        int bloodGroupId = Integer.parseInt((String) request.getParameter("bloodGroup"));
        query.setBloodGroup(bloodGroupDao.findById(bloodGroupId));

        int institutionId = Integer.parseInt((String) request.getParameter("institution"));
        query.setInstitution(institutionDao.findById(institutionId));

        int districtId = Integer.parseInt((String) request.getParameter("district"));
        query.setDistrict(districtDao.findById(districtId));

        int thanaId = Integer.parseInt((String) request.getParameter("thana"));
        query.setThana(thanaDao.findById(thanaId));

        int streetId = Integer.parseInt((String) request.getParameter("street"));
        query.setStreet(streetDao.findById(streetId));

        return query;
    }

    public String getQueryString(PatientQuery query) {
        String queryString = "SELECT patient FROM Patient patient";
        boolean isFirstCondition = true;

        Institution institution = query.getInstitution();
        Category category = query.getCategory();
        BloodGroup bloodGroup = query.getBloodGroup();
        LocalDate fromDate = query.getFromDate();
        LocalDate toDate = query.getToDate();
        District district = query.getDistrict();
        Thana thana = query.getThana();
        Street street = query.getStreet();

        if (institution != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.institution = :institution";
        }
        if (category != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.category = :category";
        }
        if (bloodGroup != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.bloodGroup = :bloodGroup";
        }
        if (fromDate != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.registrationDate >= :fromDate";
        }
        if (toDate != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.registrationDate <= :toDate";
        }
        if (district != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.institution.address.district = :district";
        }
        if (thana != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.institution.address.thana = :thana";
        }
        if (street != null) {
            if (isFirstCondition) {
                queryString += " WHERE";
                isFirstCondition = false;
            } else {
                queryString += " AND";
            }
            queryString += " patient.institution.address.street = :street";
        }

        return queryString;
    }

    public void setParametersQuery(TypedQuery<Patient> typedQuery, PatientQuery query) {
        Institution institution = query.getInstitution();
        Category category = query.getCategory();
        BloodGroup bloodGroup = query.getBloodGroup();
        LocalDate fromDate = query.getFromDate();
        LocalDate toDate = query.getToDate();
        District district = query.getDistrict();
        Thana thana = query.getThana();
        Street street = query.getStreet();

        if (institution != null) {
            typedQuery.setParameter("institution", institution);
        }
        if (category != null) {
            typedQuery.setParameter("category", category);
        }
        if (bloodGroup != null) {
            typedQuery.setParameter("bloodGroup", bloodGroup);
        }
        if (fromDate != null) {
            typedQuery.setParameter("fromDate", fromDate);
        }
        if (toDate != null) {
            typedQuery.setParameter("toDate", toDate);
        }
        if (district != null) {
            typedQuery.setParameter("district", district);
        }
        if (thana != null) {
            typedQuery.setParameter("thana", thana);
        }
        if (street != null) {
            typedQuery.setParameter("street", street);
        }
    }
}
