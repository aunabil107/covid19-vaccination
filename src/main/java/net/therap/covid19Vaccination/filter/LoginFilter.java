package net.therap.covid19Vaccination.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@WebFilter("/*")
public class LoginFilter implements javax.servlet.Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession(false);
        boolean isNullSession = Objects.isNull(session) ? true : Objects.isNull(session.getAttribute("loggedUserId"));

        if (request.getServletPath().equals("/login")) {
            if (!isNullSession) {
                RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/patient/show");
                dispatcher.forward(servletRequest, servletResponse);
            } else {
                chain.doFilter(servletRequest, servletResponse);
            }
        } else if (request.getServletPath().equals("/patient/show")) {
            if (isNullSession) {
                RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/login");
                dispatcher.forward(servletRequest, servletResponse);
            } else {
                chain.doFilter(servletRequest, servletResponse);
            }
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }
}
