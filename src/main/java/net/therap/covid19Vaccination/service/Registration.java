package net.therap.covid19Vaccination.service;

import net.therap.covid19Vaccination.model.Patient;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class Registration {

    public LocalDate getFirstDoseDate(Patient patient) {
        return LocalDate.now();
    }

    public LocalDate getSecondDoseDate(Patient patient) {
        return LocalDate.now();
    }
}
