package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.NationalIdentityDao;
import net.therap.covid19Vaccination.model.NationalIdentity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class NationalIdentityEditor extends PropertyEditorSupport {

    @Autowired
    private NationalIdentityDao nationalIdentityDao;

    @Override
    public String getAsText() {
        NationalIdentity nationalIdentity = (NationalIdentity) getValue();
        if (Objects.isNull(nationalIdentity)) {
            return "";
        }
        return String.valueOf(nationalIdentity.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            NationalIdentity nationalIdentity = nationalIdentityDao.findByNid(Integer.parseInt(idTxt));
            setValue(nationalIdentity);
        }
    }
}