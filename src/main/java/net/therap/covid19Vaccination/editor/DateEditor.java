package net.therap.covid19Vaccination.editor;

import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 11/04/2021
 **/
@Service
public class DateEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        LocalDate date = (LocalDate) getValue();
        if (Objects.isNull(date)) {
            return "";
        }
        return String.valueOf(date);
    }

    @Override
    public void setAsText(String dateText) throws IllegalArgumentException {
        if (dateText.length() > 0) {
            setValue(LocalDate.parse(dateText));
        } else {
            setValue(null);
        }
    }
}
