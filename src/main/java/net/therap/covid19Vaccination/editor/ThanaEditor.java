package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.ThanaDao;
import net.therap.covid19Vaccination.model.Thana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class ThanaEditor extends PropertyEditorSupport {

    @Autowired
    private ThanaDao thanaDao;

    @Override
    public String getAsText() {
        Thana thana = (Thana) getValue();
        if (Objects.isNull(thana)) {
            return "";
        }
        return String.valueOf(thana.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            Thana thana = thanaDao.findById(Integer.parseInt(idTxt));
            setValue(thana);
        }
    }
}
