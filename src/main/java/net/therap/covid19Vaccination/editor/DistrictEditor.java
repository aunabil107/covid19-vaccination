package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.DistrictDao;
import net.therap.covid19Vaccination.model.District;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class DistrictEditor extends PropertyEditorSupport {

    @Autowired
    private DistrictDao districtDao;

    @Override
    public String getAsText() {
        District district = (District) getValue();
        if (Objects.isNull(district)) {
            return "";
        }
        return String.valueOf(district.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            District district = districtDao.findById(Integer.parseInt(idTxt));
            setValue(district);
        }
    }
}
