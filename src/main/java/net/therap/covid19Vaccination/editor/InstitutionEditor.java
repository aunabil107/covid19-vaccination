package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.InstitutionDao;
import net.therap.covid19Vaccination.model.Institution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class InstitutionEditor extends PropertyEditorSupport {

    @Autowired
    private InstitutionDao institutionDao;

    @Override
    public String getAsText() {
        Institution institution = (Institution) getValue();
        if (Objects.isNull(institution)) {
            return "";
        }
        return String.valueOf(institution.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            Institution institution = institutionDao.findById(Integer.parseInt(idTxt));
            setValue(institution);
        }
    }
}
