package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.StreetDao;
import net.therap.covid19Vaccination.model.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class StreetEditor extends PropertyEditorSupport {

    @Autowired
    private StreetDao streetDao;

    @Override
    public String getAsText() {
        Street street = (Street) getValue();
        if (Objects.isNull(street)) {
            return "";
        }
        return String.valueOf(street.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            Street street = streetDao.findById(Integer.parseInt(idTxt));
            setValue(street);
        }
    }
}
