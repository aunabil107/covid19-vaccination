package net.therap.covid19Vaccination.editor;

import net.therap.covid19Vaccination.dao.CategoryDao;
import net.therap.covid19Vaccination.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Service
public class CategoryEditor extends PropertyEditorSupport {

    @Autowired
    private CategoryDao bategoryDao;

    @Override
    public String getAsText() {
        Category bategory = (Category) getValue();
        if (Objects.isNull(bategory)) {
            return "";
        }
        return String.valueOf(bategory.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            Category bategory = bategoryDao.findById(Integer.parseInt(idTxt));
            setValue(bategory);
        }
    }
}
