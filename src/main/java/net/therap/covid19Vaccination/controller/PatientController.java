package net.therap.covid19Vaccination.controller;

import net.therap.covid19Vaccination.dao.PatientDao;
import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.editor.*;
import net.therap.covid19Vaccination.model.*;
import net.therap.covid19Vaccination.service.Registration;
import net.therap.covid19Vaccination.utility.PatientUtility;
import net.therap.covid19Vaccination.validator.PatientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Controller
@SessionAttributes("patient")
@RequestMapping("/patient")
public class PatientController {

    private static final String PATIENT = "patient";
    private static final String PATIENT_SHOW = "patientShow";
    private static final String REDIRECT_LOGIN = "redirect:/login";
    private static final int INITIAL_STATUS = 0;

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private Registration registration;

    @Autowired
    private BloodGroupEditor bloodGroupEditor;

    @Autowired
    private CategoryEditor categoryEditor;

    @Autowired
    private DistrictEditor districtEditor;

    @Autowired
    private InstitutionEditor institutionEditor;

    @Autowired
    private NationalIdentityEditor nationalIdentityEditor;

    @Autowired
    private StreetEditor streetEditor;

    @Autowired
    private ThanaEditor thanaEditor;

    @Autowired
    private PatientValidator patientValidator;

    @Autowired
    private PatientUtility patientUtility;

    @InitBinder("patient")
    public void initBinderPatient(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

        dataBinder.registerCustomEditor(BloodGroup.class, "bloodGroup", bloodGroupEditor);
        dataBinder.registerCustomEditor(Category.class, "category", categoryEditor);
        dataBinder.registerCustomEditor(Institution.class, "institution", institutionEditor);
        dataBinder.registerCustomEditor(NationalIdentity.class, "nationalIdentity", nationalIdentityEditor);
        dataBinder.registerCustomEditor(District.class, "address.district", districtEditor);
        dataBinder.registerCustomEditor(Thana.class, "address.thana", thanaEditor);
        dataBinder.registerCustomEditor(Street.class, "address.street", streetEditor);

        dataBinder.addValidators(patientValidator);
    }

    @GetMapping
    public String create(@RequestParam int id, Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int loggedUserId = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(loggedUserId));
        } else {
            model.addAttribute("user", new User());
        }

        Patient patient;
        if (id == 0) {
            patient = new Patient();
        } else {
            int loggedId = (int) session.getAttribute("loggedUserId");
            User user = userDao.findById(loggedId);
            if (user.getPatient().getId() != id) {
                return PATIENT;
            }
            patient = patientDao.findById(id);
        }

        patientUtility.setupModel(patient, model);
        return PATIENT;
    }

    @PostMapping
    public String save(@Valid @ModelAttribute Patient patient, BindingResult bindingResult, Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int id = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(id));
        } else {
            model.addAttribute("user", new User());
        }

        if (bindingResult.hasErrors()) {
            patientUtility.setupModel(patient, model);
            return PATIENT;
        }

        if (patient.isNew()) {
            patient.setRegistrationDate(LocalDate.now());
            patient.setVaccinationDose1Date(registration.getFirstDoseDate(patient));
            patient.setVaccinationDose2Date(registration.getSecondDoseDate(patient));
            patient.setVaccinationStatus(INITIAL_STATUS);
        }

        patientDao.save(patient);
        return REDIRECT_LOGIN;
    }

    @GetMapping("/show")
    public String show(Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int id = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(id));
        } else {
            model.addAttribute("user", new User());
        }

        int id = (int) session.getAttribute("loggedUserId");
        model.addAttribute("patient", userDao.findById(id).getPatient());

        return PATIENT_SHOW;
    }
}