package net.therap.covid19Vaccination.controller;

import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 11/04/2021
 **/
@Controller
public class HomeController {

    @Autowired
    private UserDao userDao;

    @GetMapping("/")
    public String home(Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int id = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(id));
        } else {
            model.addAttribute("user", new User());
        }
        return "home";
    }
}
