package net.therap.covid19Vaccination.controller;

import net.therap.covid19Vaccination.dao.PatientDao;
import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.editor.*;
import net.therap.covid19Vaccination.model.*;
import net.therap.covid19Vaccination.utility.PatientUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 11/04/2021
 **/
@Controller
public class PatientQueryController {

    private static final String QUERY = "query";
    private static final String STATS = "stats";

    @Autowired
    private PatientUtility patientUtility;

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private BloodGroupEditor bloodGroupEditor;

    @Autowired
    private CategoryEditor categoryEditor;

    @Autowired
    private DistrictEditor districtEditor;

    @Autowired
    private InstitutionEditor institutionEditor;

    @Autowired
    private NationalIdentityEditor nationalIdentityEditor;

    @Autowired
    private StreetEditor streetEditor;

    @Autowired
    private ThanaEditor thanaEditor;

    @Autowired
    private DateEditor dateEditor;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

        dataBinder.registerCustomEditor(BloodGroup.class, "bloodGroup", bloodGroupEditor);
        dataBinder.registerCustomEditor(Category.class, "category", categoryEditor);
        dataBinder.registerCustomEditor(Institution.class, "institution", institutionEditor);
        dataBinder.registerCustomEditor(District.class, "district", districtEditor);
        dataBinder.registerCustomEditor(Thana.class, "thana", thanaEditor);
        dataBinder.registerCustomEditor(Street.class, "street", streetEditor);
        dataBinder.registerCustomEditor(LocalDate.class, "fromDate", dateEditor);
        dataBinder.registerCustomEditor(LocalDate.class, "toDate", dateEditor);
    }

    @GetMapping("/query")
    public String query(Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int id = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(id));
        } else {
            model.addAttribute("user", new User());
        }

        patientUtility.setupQueryModel(model);
        model.addAttribute("patientQuery", new PatientQuery());

        return QUERY;
    }

    @PostMapping("/query")
    public String showStats(@ModelAttribute("patientQuery") PatientQuery query, Model model, HttpSession session) {
        if (Objects.nonNull(session.getAttribute("loggedUserId"))) {
            int id = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(id));
        } else {
            model.addAttribute("user", new User());
        }

        model.addAttribute("patientList", patientDao.query(query));
        return STATS;
    }
}
