package net.therap.covid19Vaccination.controller;

import net.therap.covid19Vaccination.dao.*;
import net.therap.covid19Vaccination.editor.DistrictEditor;
import net.therap.covid19Vaccination.editor.StreetEditor;
import net.therap.covid19Vaccination.editor.ThanaEditor;
import net.therap.covid19Vaccination.model.District;
import net.therap.covid19Vaccination.model.Institution;
import net.therap.covid19Vaccination.model.Street;
import net.therap.covid19Vaccination.model.Thana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author aunabil.chakma
 * @since 13/04/2021
 **/
@Controller
@RequestMapping("/institution")
public class InstitutionController {

    private static String INSTITUTION = "institution";
    private static String INSTITUTION_LIST = "institutionList";
    private static String REDIRECT_INSTITUTION_LIST = "redirect:/institution/list";

    @Autowired
    private UserDao userDao;

    @Autowired
    private InstitutionDao institutionDao;

    @Autowired
    private DistrictDao districtDao;

    @Autowired
    private ThanaDao thanaDao;

    @Autowired
    private StreetDao streetDao;

    @Autowired
    private DistrictEditor districtEditor;

    @Autowired
    private StreetEditor streetEditor;

    @Autowired
    private ThanaEditor thanaEditor;

    @InitBinder("institution")
    public void initBinderPatient(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

        dataBinder.registerCustomEditor(District.class, "address.district", districtEditor);
        dataBinder.registerCustomEditor(Thana.class, "address.thana", thanaEditor);
        dataBinder.registerCustomEditor(Street.class, "address.street", streetEditor);
    }

    @GetMapping
    public String get(@RequestParam int id, Model model, HttpSession session) {
        Institution institution = (id == 0) ? new Institution() : institutionDao.findById(id);

        model.addAttribute("institution", institution);
        model.addAttribute("districtList", districtDao.findAll());
        model.addAttribute("thanaList", thanaDao.findAll());
        model.addAttribute("streetList", streetDao.findAll());

        int userId = (int) session.getAttribute("loggedUserId");
        model.addAttribute("user", userDao.findById(userId));

        return INSTITUTION;
    }

    @PostMapping
    public String save(@Valid @ModelAttribute Institution institution, BindingResult bindingResult, Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("institution", institution);
            model.addAttribute("districtList", districtDao.findAll());
            model.addAttribute("thanaList", thanaDao.findAll());
            model.addAttribute("streetList", streetDao.findAll());

            int userId = (int) session.getAttribute("loggedUserId");
            model.addAttribute("user", userDao.findById(userId));

            return INSTITUTION;
        }
        institutionDao.save(institution);
        return REDIRECT_INSTITUTION_LIST;
    }

    @GetMapping("/list")
    public String list(Model model, HttpSession session) {
        model.addAttribute("institutionList", institutionDao.findAll());

        int userId = (int) session.getAttribute("loggedUserId");
        model.addAttribute("user", userDao.findById(userId));

        return INSTITUTION_LIST;
    }
}
