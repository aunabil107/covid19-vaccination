package net.therap.covid19Vaccination.controller;

import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.model.User;
import net.therap.covid19Vaccination.validator.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Controller
public class LoginController {

    private static final String LOGIN_PAGE = "login";
    private static final String REDIRECT_SUCCESS = "redirect:/patient/show";
    private static final String REDIRECT_LOGIN = "redirect:/login";
    private static final String REDIRECT_HOME = "redirect:/";
    private static final String LOGGED_USER_ID = "loggedUserId";

    @Autowired
    private UserDao userDao;

    @Autowired
    private LoginValidator loginValidator;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        dataBinder.addValidators(loginValidator);
    }

    @GetMapping("/login")
    public String show(Model model) {
        model.addAttribute("user", new User());
        return LOGIN_PAGE;
    }

    @PostMapping("/login")
    public String check(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return LOGIN_PAGE;
        }

        user = userDao.findByUsername(user.getUsername());
        session.setAttribute(LOGGED_USER_ID, user.getId());

        if (user.getIsAdmin() == 0) {
            return REDIRECT_SUCCESS;
        } else {
            return REDIRECT_HOME;
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        if (Objects.nonNull(session.getAttribute(LOGGED_USER_ID))) {
            session.removeAttribute(LOGGED_USER_ID);
        }
        return REDIRECT_LOGIN;
    }
}
