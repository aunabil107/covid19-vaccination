package net.therap.covid19Vaccination.validator;

import net.therap.covid19Vaccination.dao.NationalIdentityDao;
import net.therap.covid19Vaccination.dao.PatientDao;
import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.model.NationalIdentity;
import net.therap.covid19Vaccination.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 29/03/2021
 **/
@Component
public class PatientValidator implements Validator {

    @Autowired
    private NationalIdentityDao nationalIdentityDao;

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private UserDao userDao;

    public boolean supports(Class clazz) {
        return Patient.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        Patient patient = (Patient) obj;
        NationalIdentity nationalIdentity = patient.getNationalIdentity();

        if (patient.isNew()) {
            if (Objects.isNull(nationalIdentity)) {
                e.rejectValue("nationalIdentity", "wrong nid", "nid is invalid");
            } else if (Objects.nonNull(patientDao.findByNationalIdentity(nationalIdentity))) {
                e.rejectValue("nationalIdentity", "registered already", "registered already");
            } else if (Objects.nonNull(userDao.findByUsername(patient.getUser().getUsername()))) {
                e.rejectValue("username", "used username", "username already used");
            }
        }
    }
}