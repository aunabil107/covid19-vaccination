package net.therap.covid19Vaccination.validator;

import net.therap.covid19Vaccination.dao.UserDao;
import net.therap.covid19Vaccination.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 01/04/2021
 **/
@Component
public class LoginValidator implements Validator {

    @Autowired
    private UserDao userDao;

    public boolean supports(Class clazz) {
        return User.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        User user = (User) obj;

        if (Objects.isNull(userDao.findByUsernameAndPassword(user.getUsername(), user.getPassword()))) {
            e.reject("wrong username/password", "wrong username/password");
        }
    }
}
