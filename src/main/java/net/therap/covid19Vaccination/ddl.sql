CREATE SCHEMA covid19_vaccination;

CREATE TABLE covid19_vaccination.district
(
    id   INT          NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE covid19_vaccination.thana
(
    id          INT          NOT NULL AUTO_INCREMENT,
    name        VARCHAR(100) NOT NULL,
    district_id INT          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (district_id) REFERENCES covid19_vaccination.district (id)
);

CREATE TABLE covid19_vaccination.street
(
    id       INT          NOT NULL AUTO_INCREMENT,
    name     VARCHAR(100) NOT NULL,
    thana_id INT          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (thana_id) REFERENCES covid19_vaccination.thana (id)
);

CREATE TABLE covid19_vaccination.address
(
    id          INT NOT NULL AUTO_INCREMENT,
    street_id   INT NOT NULL,
    thana_id    INT NOT NULL,
    district_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (street_id) REFERENCES covid19_vaccination.street (id),
    FOREIGN KEY (thana_id) REFERENCES covid19_vaccination.thana (id),
    FOREIGN KEY (district_id) REFERENCES covid19_vaccination.district (id)
);

CREATE TABLE covid19_vaccination.institution
(
    id         INT          NOT NULL AUTO_INCREMENT,
    name       VARCHAR(100) NOT NULL,
    address_id INT          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES covid19_vaccination.address (id)
);

CREATE TABLE covid19_vaccination.national_identity
(
    id         INT          NOT NULL AUTO_INCREMENT,
    name       VARCHAR(100) NOT NULL,
    birth_date DATE         NOT NULL,
    nid        INT          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE covid19_vaccination.category
(
    id   INT          NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE covid19_vaccination.blood_group
(
    id   INT          NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE covid19_vaccination.patient
(
    id                     INT          NOT NULL AUTO_INCREMENT,
    username               VARCHAR(100),
    password               VARCHAR(100),
    email                  VARCHAR(100) NOT NULL,
    phone                  VARCHAR(100) NOT NULL,
    registration_date      DATE,
    vaccination_dose1_date DATE,
    vaccination_dose2_date DATE,
    vaccination_status     INT,
    blood_group_id         INT          NOT NULL,
    institution_id         INT          NOT NULL,
    national_identity_id   INT          NOT NULL,
    category_id            INT          NOT NULL,
    address_id             INT          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (blood_group_id) REFERENCES covid19_vaccination.blood_group (id),
    FOREIGN KEY (institution_id) REFERENCES covid19_vaccination.institution (id),
    FOREIGN KEY (national_identity_id) REFERENCES covid19_vaccination.national_identity (id),
    FOREIGN KEY (category_id) REFERENCES covid19_vaccination.category (id),
    FOREIGN KEY (address_id) REFERENCES covid19_vaccination.address (id)
);

INSERT INTO `covid19_vaccination`.`district` (`name`)
VALUES ('dhaka');
INSERT INTO `covid19_vaccination`.`thana` (`name`, `district_id`)
VALUES ('mirpur', '1');
INSERT INTO `covid19_vaccination`.`street` (`name`, `thana_id`)
VALUES ('shewrapara', '1');
INSERT INTO `covid19_vaccination`.`address` (`street_id`, `thana_id`, `district_id`)
VALUES ('1', '1', '1');
INSERT INTO `covid19_vaccination`.`institution` (`name`, `address_id`)
VALUES ('Mirpur Hospital', '1');
INSERT INTO `covid19_vaccination`.`institution` (`name`, `address_id`)
VALUES ('Shewrapara Hospital', '1');
INSERT INTO `covid19_vaccination`.`blood_group` (`name`)
VALUES ('B+');
INSERT INTO `covid19_vaccination`.`category` (`name`)
VALUES ('age-40+ civilian');
INSERT INTO `covid19_vaccination`.`national_identity` (`name`, `birth_date`, `nid`)
VALUES ('Aunabil', '2020-10-10', '1234');

