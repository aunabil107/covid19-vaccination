package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * @author aunabil.chakma
 * @since 11/04/2021
 **/
@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public User findById(int id) {
        return em.find(User.class, id);
    }

    public User findByUsername(String username) {
        TypedQuery<User> query = em.createQuery("SELECT user FROM User user WHERE user.username = :username", User.class);
        query.setParameter("username", username);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findByUsernameAndPassword(String username, String password) {
        TypedQuery<User> query = em.createQuery("SELECT user FROM User user WHERE user.username = :username AND user.password = :password", User.class);
        query.setParameter("username", username);
        query.setParameter("password", password);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
