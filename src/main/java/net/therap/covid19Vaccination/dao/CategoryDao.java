package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class CategoryDao {

    @PersistenceContext
    private EntityManager em;

    public Category findById(int id) {
        return em.find(Category.class, id);
    }

    public List<Category> findAll() {
        TypedQuery<Category> query = em.createQuery("SELECT category FROM Category category", Category.class);

        return query.getResultList();
    }
}
