package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.Address;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author aunabil.chakma
 * @since 01/04/2021
 **/
@Repository
public class AddressDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Address address) {
        if (address.getId() == 0) {
            em.persist(address);
        } else {
            em.merge(address);
        }
    }
}