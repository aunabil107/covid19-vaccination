package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.NationalIdentity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class NationalIdentityDao {

    @PersistenceContext
    private EntityManager em;

    public NationalIdentity findById(int id) {
        return em.find(NationalIdentity.class, id);
    }

    public NationalIdentity findByNid(int nid) {
        TypedQuery<NationalIdentity> query = em.createQuery("SELECT nationalIdentity FROM NationalIdentity nationalIdentity WHERE nationalIdentity.nid = :nid", NationalIdentity.class);
        query.setParameter("nid", nid);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
