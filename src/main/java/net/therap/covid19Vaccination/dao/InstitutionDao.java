package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.Institution;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class InstitutionDao {

    @PersistenceContext
    private EntityManager em;

    public Institution findById(int id) {
        return em.find(Institution.class, id);
    }

    public List<Institution> findAll() {
        TypedQuery<Institution> query = em.createQuery("SELECT institution FROM Institution institution", Institution.class);

        return query.getResultList();
    }

    @Transactional
    public void save(Institution institution) {
        if (institution.getId() == 0) {
            em.persist(institution);
        } else {
            em.merge(institution);
        }
    }
}
