package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.District;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class DistrictDao {

    @PersistenceContext
    private EntityManager em;

    public District findById(int id) {
        return em.find(District.class, id);
    }

    public List<District> findAll() {
        TypedQuery<District> query = em.createQuery("SELECT district FROM District district", District.class);

        return query.getResultList();
    }
}
