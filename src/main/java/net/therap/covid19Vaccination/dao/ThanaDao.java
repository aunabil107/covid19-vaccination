package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.Thana;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class ThanaDao {

    @PersistenceContext
    private EntityManager em;

    public Thana findById(int id) {
        return em.find(Thana.class, id);
    }

    public List<Thana> findAll() {
        TypedQuery<Thana> query = em.createQuery("SELECT thana FROM Thana thana", Thana.class);

        return query.getResultList();
    }
}
