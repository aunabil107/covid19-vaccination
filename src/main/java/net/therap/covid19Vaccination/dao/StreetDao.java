package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.Street;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class StreetDao {

    @PersistenceContext
    private EntityManager em;

    public Street findById(int id) {
        return em.find(Street.class, id);
    }

    public List<Street> findAll() {
        TypedQuery<Street> query = em.createQuery("SELECT street FROM Street street", Street.class);

        return query.getResultList();
    }
}
