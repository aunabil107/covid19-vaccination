package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.NationalIdentity;
import net.therap.covid19Vaccination.model.Patient;
import net.therap.covid19Vaccination.model.PatientQuery;
import net.therap.covid19Vaccination.utility.PatientUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class PatientDao {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PatientUtility patientUtility;

    public Patient findById(int id) {
        return em.find(Patient.class, id);
    }

    public Patient findByNationalIdentity(NationalIdentity nationalIdentity) {
        TypedQuery<Patient> query = em.createQuery("SELECT patient FROM Patient patient WHERE patient.nationalIdentity = :nationalIdentity", Patient.class);
        query.setParameter("nationalIdentity", nationalIdentity);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Patient> query(PatientQuery query) {
        String queryString = patientUtility.getQueryString(query);
        TypedQuery<Patient> typedQuery = em.createQuery(queryString, Patient.class);
        patientUtility.setParametersQuery(typedQuery, query);

        return typedQuery.getResultList();
    }

    @Transactional
    public void save(Patient patient) {
        if (patient.isNew()) {
            em.persist(patient);
        } else {
            em.merge(patient);
        }
    }
}