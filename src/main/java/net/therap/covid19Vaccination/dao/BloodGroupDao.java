package net.therap.covid19Vaccination.dao;

import net.therap.covid19Vaccination.model.BloodGroup;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 28/03/2021
 **/
@Repository
public class BloodGroupDao {

    @PersistenceContext
    private EntityManager em;

    public BloodGroup findById(int id) {
        return em.find(BloodGroup.class, id);
    }

    public List<BloodGroup> findAll() {
        TypedQuery<BloodGroup> query = em.createQuery("SELECT bloodGroup FROM BloodGroup bloodGroup", BloodGroup.class);

        return query.getResultList();
    }
}
