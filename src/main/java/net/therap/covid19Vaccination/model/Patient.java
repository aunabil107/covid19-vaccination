package net.therap.covid19Vaccination.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author aunabil.chakma
 * @since 25/03/2021
 **/
@Entity
@Table(name = "patient")
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = false)
    private String email;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = false)
    private String phone;

    @Column(name = "registration_date")
    private LocalDate registrationDate;

    @Column(name = "vaccination_dose1_date")
    private LocalDate vaccinationDose1Date;

    @Column(name = "vaccination_dose2_date")
    private LocalDate vaccinationDose2Date;

    @Column(name = "vaccination_status")
    private int vaccinationStatus;

    @ManyToOne
    @JoinColumn(name = "blood_group_id", nullable = false)
    private BloodGroup bloodGroup;

    @ManyToOne
    @JoinColumn(name = "institution_id", nullable = false, updatable = false)
    private Institution institution;

    @OneToOne
    @JoinColumn(name = "national_identity_id", unique = true, nullable = false, updatable = false)
    private NationalIdentity nationalIdentity;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDate getVaccinationDose1Date() {
        return vaccinationDose1Date;
    }

    public void setVaccinationDose1Date(LocalDate vaccinationDose1Date) {
        this.vaccinationDose1Date = vaccinationDose1Date;
    }

    public LocalDate getVaccinationDose2Date() {
        return vaccinationDose2Date;
    }

    public void setVaccinationDose2Date(LocalDate vaccinationDose2Date) {
        this.vaccinationDose2Date = vaccinationDose2Date;
    }

    public int getVaccinationStatus() {
        return vaccinationStatus;
    }

    public void setVaccinationStatus(int vaccinationStatus) {
        this.vaccinationStatus = vaccinationStatus;
    }

    public BloodGroup getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(BloodGroup bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public NationalIdentity getNationalIdentity() {
        return nationalIdentity;
    }

    public void setNationalIdentity(NationalIdentity nationalIdentity) {
        this.nationalIdentity = nationalIdentity;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isNew() {
        return this.id == 0;
    }
}
