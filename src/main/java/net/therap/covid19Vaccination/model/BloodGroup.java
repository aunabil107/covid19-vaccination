package net.therap.covid19Vaccination.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author aunabil.chakma
 * @since 25/03/2021
 **/
@Entity
@Table(name = "blood_group")
public class BloodGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    public BloodGroup() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
