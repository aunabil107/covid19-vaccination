package net.therap.covid19Vaccination.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author aunabil.chakma
 * @since 25/03/2021
 **/
@Entity
@Table(name = "street")
public class Street implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "thana_id")
    private Thana thana;

    public Street() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Thana getThana() {
        return thana;
    }

    public void setThana(Thana thana) {
        this.thana = thana;
    }
}
