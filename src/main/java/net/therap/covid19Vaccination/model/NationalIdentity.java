package net.therap.covid19Vaccination.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author aunabil.chakma
 * @since 25/03/2021
 **/
@Entity
@Table(name = "national_identity")
public class NationalIdentity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column
    private int nid;

    @OneToOne(mappedBy = "nationalIdentity")
    private Patient patient;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getNid() {
        return nid;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return String.valueOf(this.nid);
    }
}
