<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 10/04/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <c:url value="/css/style.css" var="styleSheetLink"/>
    <link href="${styleSheetLink}" rel="stylesheet">

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>

</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #3366cc;">
    <div class="container-fluid">
        <c:url value="/" var="homeLink"/>
        <a class="navbar-brand" href="${homeLink}">Covid-19 Vaccination</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ms-auto mb-2 mb-md-0 text-capitalize">
                <li class="nav-item">
                    <c:url value="/" var="homeLink"/>
                    <a class="nav-link" href="${homeLink}" id="home">Home</a>
                </li>
                <li class="nav-item">
                    <c:url value="/patient" var="registerLink">
                        <c:param name="id" value="0"/>
                    </c:url>
                    <a class="nav-link" href="${registerLink}" id="register">Register</a>
                </li>
                <li class="nav-item">
                    <c:url value="/query" var="statsLink"/>
                    <a class="nav-link" href="${statsLink}" id="stats">Stats</a>
                </li>

                <c:if test="${user.isAdmin==1}">
                    <li class="nav-item">
                        <c:url value="/institution/list" var="institutionLink"/>
                        <a class="nav-link" href="${institutionLink}" id="institution">Institution</a>
                    </li>
                    <li class="nav-item">
                        <c:url value="/bloodGroup/list" var="bloodGroupLink"/>
                        <a class="nav-link" href="${bloodGroupLink}" id="bloodGroup">BloodGroup</a>
                    </li>
                    <li class="nav-item">
                        <c:url value="/Category/list" var="categoryLink"/>
                        <a class="nav-link" href="${categoryLink}" id="category">Category</a>
                    </li>
                </c:if>
                <c:choose>
                    <c:when test="${user.id==0}">
                        <li class="nav-item">
                            <c:url value="/login" var="loginLink"/>
                            <a class="nav-link" href="${loginLink}" id="login">Login</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="nav-item">
                            <c:url value="/patient/show" var="profileLink"/>
                            <c:if test="${user.isAdmin==0}">
                                <a class="nav-link" href="${profileLink}" id="profile"><c:out
                                        value="${user.username}"/></a>
                            </c:if>
                            <c:if test="${user.isAdmin==1}">
                                <a class="nav-link" href="#" id="profile"><c:out
                                        value="${user.username}"/>(Admin)</a>
                            </c:if>
                        </li>
                        <li class="nav-item">
                            <c:url value="/logout" var="logoutLink"/>
                            <a class="nav-link" href="${logoutLink}" id="logout">Logout</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</nav>

<main class="container">
    <sitemesh:write property='body'/>
</main>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
        integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"
        integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc"
        crossorigin="anonymous"></script>

</body>
</html>

