<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
   @author aunabil.chakma
   @since 29/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Details</title>
</head>
<body>


<h5 class="text-center">Your Registered Details</h5>

<div class="p-5 rounded" style="background-color: #e3f2fd;">
    <table class="table">
        <tbody>
        <tr>
            <td><c:out value="Name"/></td>
            <td><c:out value="${patient.nationalIdentity.name}"/></td>
        </tr>
        <tr>
            <td><c:out value="NID"/></td>
            <td><c:out value="${patient.nationalIdentity}"/></td>
        </tr>
        <tr>
            <td><c:out value="Email"/></td>
            <td><c:out value="${patient.email}"/></td>
        </tr>
        <tr>
            <td><c:out value="Phone"/></td>
            <td><c:out value="${patient.phone}"/></td>
        </tr>
        <tr>
            <td><c:out value="Username"/></td>
            <td><c:out value="${patient.user.username}"/></td>
        </tr>
        <tr>
            <td><c:out value="Password"/></td>
            <td><c:out value="${patient.user.password}"/></td>
        </tr>
        <tr>
            <td><c:out value="Category"/></td>
            <td><c:out value="${patient.category}"/></td>
        </tr>
        <tr>
            <td><c:out value="BLoodGroup"/></td>
            <td><c:out value="${patient.bloodGroup}"/></td>
        </tr>
        <tr>
            <td><c:out value="Institution"/></td>
            <td><c:out value="${patient.institution}"/></td>
        </tr>
        <tr>
            <td><c:out value="Address"/></td>
            <td><c:out value="${patient.address}"/></td>
        </tr>
        </tbody>
    </table>
</div>

<c:url value="/patient" var="updateLink">
    <c:param name="id" value="${patient.id}"/>
</c:url>
<a class="btn btn-primary" href="${updateLink}" role="button">Update</a>

<script>
    setActivePage();

    function setActivePage() {
        document.getElementById("profile").classList.add("active");
    }
</script>

</body>
</html>
