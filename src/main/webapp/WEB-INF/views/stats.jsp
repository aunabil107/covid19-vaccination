<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 04/04/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Stats</title>
</head>
<body>

<h5 class="text-center"><c:out value="Total patients: ${patientList.size()}"/></h5>

<div class="p-5 rounded" style="background-color: #e3f2fd;">
    <table class="table">
        <thead>
        <tr class="table-light">
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Institution</th>
            <th scope="col">Registration Date</th>
            <th scope="col">Address</th>
        </tr>
        </thead>
        <tbody>
        <c:set value="1" var="rowNumber"/>

        <c:forEach var="patient" items="${patientList}">
            <tr class="table-light">
                <th scope="row">${rowNumber}</th>
                <td><c:out value="${patient.nationalIdentity.name}"/></td>
                <td><c:out value="${patient.institution}"/></td>
                <td><c:out value="${patient.registrationDate}"/></td>
                <td><c:out value="${patient.address}"/></td>
            </tr>
            <c:set value="${rowNumber+1}" var="rowNumber"/>
        </c:forEach>
        </tbody>
    </table>
</div>

<script>
    setActivePage();

    function setActivePage() {
        document.getElementById("stats").classList.add("active");
    }
</script>
</body>
</html>
