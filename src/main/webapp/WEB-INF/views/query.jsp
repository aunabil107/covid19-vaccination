<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 29/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Query</title>
</head>
<body>

<c:url var="link" value="/query"/>

<div class="p-5 rounded" style="background-color: #e3f2fd;">
    <form:form method="post" action="${link}" modelAttribute="patientQuery">

        <div class="mb-3">
            <form:label path="fromDate" cssClass="form-label">From</form:label>
            <form:errors path="fromDate"/>
            <form:input path="fromDate" cssClass="form-control" id="fromDate" type="date"/>
        </div>

        <div class="mb-3">
            <form:label path="toDate" cssClass="form-label">To</form:label>
            <form:errors path="toDate"/>
            <form:input path="toDate" cssClass="form-control" id="toDate" type="date"/>
        </div>

        <div class="mb-3">
            <form:label path="category" cssClass="form-label">Category</form:label>
            <form:errors path="category"/>
            <form:select path="category" cssClass="form-select" id="category">
                <form:option value="0" label="--"/>
                <form:options items="${categoryList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="bloodGroup" cssClass="form-label">BloodGroup</form:label>
            <form:errors path="bloodGroup"/>
            <form:select path="bloodGroup" cssClass="form-select" id="bloodGroup">
                <form:option value="0" label="--"/>
                <form:options items="${bloodGroupList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="institution" cssClass="form-label">Institution</form:label>
            <form:errors path="institution"/>
            <form:select path="institution" cssClass="form-select" id="institution">
                <form:option value="0" label="--"/>
                <form:options items="${institutionList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="district" cssClass="form-label">District</form:label>
            <form:errors path="district"/>
            <form:select path="district" cssClass="form-select" id="districts" onchange="changeThana();">
                <form:option value="0" label="--"/>
                <form:options items="${districtList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="thana" cssClass="form-label">Thana</form:label>
            <form:errors path="thana"/>
            <form:select path="thana" cssClass="form-select" id="thanas" onchange="changeStreet();">
                <form:options items="${thanaList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="street" cssClass="form-label">Street</form:label>
            <form:errors path="street"/>
            <form:select path="street" cssClass="form-select" id="streets">
                <form:options items="${streetList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <form:button type="submit" class="btn btn-primary">Search</form:button>
    </form:form>
</div>

<script type="text/javascript">
    changeThana();
    changeStreet();
    setActivePage();

    function changeThana() {
        let districtId = document.getElementById("districts").value;
        let thanaElement = document.getElementById("thanas");
        thanaElement.options.length = 0;
        thanaElement.options[0] = new Option("--", "0", false, false);

        <c:forEach var="district" items="${districtList}">
        if (districtId == "${district.id}") {
            let index = 1;
            <c:forEach var="thana" items="${district.thanaList}">
            thanaElement.options[index] = new Option("${thana.name}", "${thana.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
        changeStreet();
    }

    function changeStreet() {
        let thanaId = document.getElementById("thanas").value;
        let streetElement = document.getElementById("streets");
        streetElement.options.length = 0;
        streetElement.options[0] = new Option("--", "0", false, false);

        <c:forEach var="thana" items="${thanaList}">
        if (thanaId == "${thana.id}") {
            let index = 1;
            <c:forEach var="street" items="${thana.streetList}">
            streetElement.options[index] = new Option("${street.name}", "${street.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
    }

    function setActivePage() {
        document.getElementById("stats").classList.add("active");
    }

</script>
</body>
</html>
