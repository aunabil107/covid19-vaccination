<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 29/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<c:url value="/patient" var="registrationLink"/>
<h5 class="text-center" id="title_message">Covid-19 Registration Form</h5>

<div class="p-5 rounded" style="background-color: #e3f2fd;">

    <form:form modelAttribute="patient" method="post" action="${registrationLink}">

        <form:hidden path="id"/>

        <div class="mb-3">
            <form:label path="email" cssClass="form-label">Email</form:label>
            <form:errors path="email"/>
            <form:input path="email" cssClass="form-control" id="email" type="email"/>
        </div>

        <div class="mb-3">
            <form:label path="phone" cssClass="form-label">Phone</form:label>
            <form:errors path="phone"/>
            <form:input path="phone" cssClass="form-control" id="phone"/>
        </div>

        <div class="mb-3">
            <form:label path="user.username" cssClass="form-label">Username</form:label>
            <form:errors path="user.username"/>
            <form:input path="user.username" cssClass="form-control" id="username"/>
        </div>

        <div class="mb-3">
            <form:label path="user.password" cssClass="form-label">Password</form:label>
            <form:errors path="user.password"/>
            <form:password path="user.password" cssClass="form-control" id="password" value="${patient.user.password}"/>
        </div>

        <div class="mb-3">
            <form:label path="nationalIdentity" cssClass="form-label">National ID</form:label>
            <form:errors path="nationalIdentity"/>
            <form:input path="nationalIdentity" cssClass="form-control" id="national_identity"
                        value="${patient.nationalIdentity.nid}"/>
        </div>

        <div class="mb-3">
            <form:label path="category" cssClass="form-label">Category</form:label>
            <form:errors path="category"/>
            <form:select path="category" cssClass="form-select" id="category">
                <form:options items="${categoryList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="bloodGroup" cssClass="form-label">BloodGroup</form:label>
            <form:errors path="bloodGroup"/>
            <form:select path="bloodGroup" cssClass="form-select" id="blood_group">
                <form:options items="${bloodGroupList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="institution" cssClass="form-label">Institution</form:label>
            <form:errors path="institution"/>
            <form:select path="institution" cssClass="form-select" id="institution">
                <form:options items="${institutionList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.district" cssClass="form-label">District</form:label>
            <form:errors path="address.district"/>
            <form:select path="address.district" cssClass="form-select" id="districts" onchange="changeThana();">
                <form:options items="${districtList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.thana" cssClass="form-label">Thana</form:label>
            <form:errors path="address.thana"/>
            <form:select path="address.thana" cssClass="form-select" id="thanas" onchange="changeStreet();">
                <form:options items="${thanaList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.street" cssClass="form-label">Street</form:label>
            <form:errors path="address.street"/>
            <form:select path="address.street" cssClass="form-select" id="streets">
                <form:options items="${streetList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <form:button type="submit" class="btn btn-primary"><c:out value="${action}"/></form:button>
    </form:form>

</div>

<script type="text/javascript">

    setActivePage();
    changeThana();
    setDisabledInputs();
    getLocation();


    function changeThana() {
        let districtId = document.getElementById("districts").value;
        let thanaElement = document.getElementById("thanas");
        thanaElement.options.length = 0;

        <c:forEach var="district" items="${districtList}">
        if (districtId == "${district.id}") {
            let index = 0;
            <c:forEach var="thana" items="${district.thanaList}">
            thanaElement.options[index] = new Option("${thana.name}", "${thana.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
        changeStreet();
    }

    function changeStreet() {
        let thanaId = document.getElementById("thanas").value;
        let streetElement = document.getElementById("streets");
        streetElement.options.length = 0;

        <c:forEach var="thana" items="${thanaList}">
        if (thanaId == "${thana.id}") {
            let index = 0;
            <c:forEach var="street" items="${thana.streetList}">
            streetElement.options[index] = new Option("${street.name}", "${street.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
    }

    function setActivePage() {
        document.getElementById("register").classList.add("active");
    }

    function setDisabledInputs() {
        <c:if test="${patient.id!=0}">
        document.getElementById("username").setAttribute("disabled", "true");
        document.getElementById("national_identity").setAttribute("disabled", "true");
        document.getElementById("institution").setAttribute("disabled", "true");
        document.getElementById("title_message").innerText = "Update your registration info.";
        </c:if>
    }

    function getLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                let latitude = position.coords.latitude;
                let longitude = position.coords.longitude;

                let arr = [];
                <c:forEach var="institution" items="${institutionList}">
                arr.push([Math.sqrt(Math.pow(latitude -${institution.address.latitude}, 2) +
                    Math.pow(longitude -${institution.address.longitude}, 2))
                    , "${institution.id}", "${institution.name}"]);
                </c:forEach>
                arr.sort(function (a, b) {
                    return a[0] - b[0];
                });

                let institutionElement = document.getElementById("institution");
                institutionElement.options.length = 0;

                <c:forEach var="i" begin="0" end="${numberOfHospital-1}">
                institutionElement.options[${i}] = new Option(arr[${i}][2], arr[${i}][1], false, false);
                </c:forEach>
            });
        }
    }
</script>
</body>
</html>
