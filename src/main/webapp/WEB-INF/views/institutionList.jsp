<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 13/04/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Institutions</title>
</head>
<body>

<h5 class="text-center"><c:out value="Total institutions: ${institutionList.size()}"/></h5>

<div class="p-5 rounded" style="background-color: #e3f2fd;">
    <table class="table">
        <thead>
        <tr class="table-light">
            <th scope="col">#</th>
            <th scope="col">Institution</th>
            <th scope="col">Address</th>
        </tr>
        </thead>
        <tbody>
        <c:set value="1" var="rowNumber"/>

        <c:forEach var="institution" items="${institutionList}">
            <c:url var="updateLink" value="/institution">
                <c:param name="id" value="${institution.id}"/>
            </c:url>
            <tr class="table-light">
                <th scope="row">${rowNumber}</th>
                <td><a href="${updateLink}"><c:out value="${institution.name}"/></a></td>
                <td><c:out value="${institution.address}"/></td>
            </tr>
            <c:set value="${rowNumber+1}" var="rowNumber"/>
        </c:forEach>
        </tbody>
    </table>

    <c:url value="/institution" var="createLink">
        <c:param name="id" value="0"/>
    </c:url>
    <a class="btn btn-primary" href="${createLink}" role="button">Add</a>
</div>


</body>
</html>
