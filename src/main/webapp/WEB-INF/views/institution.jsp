<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 13/04/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Institution</title>
</head>
<body>

<c:url value="/institution" var="saveLink"/>

<div class="p-5 rounded" style="background-color: #e3f2fd;">

    <form:form modelAttribute="institution" method="post" action="${saveLink}">

        <form:hidden path="id"/>

        <div class="mb-3">
            <form:label path="name" cssClass="form-label">Name</form:label>
            <form:errors path="name"/>
            <form:input path="name" cssClass="form-control" id="name"/>
        </div>

        <div class="mb-3">
            <form:label path="address.district" cssClass="form-label">District</form:label>
            <form:errors path="address.district"/>
            <form:select path="address.district" cssClass="form-select" id="districts" onchange="changeThana();">
                <form:options items="${districtList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.thana" cssClass="form-label">Thana</form:label>
            <form:errors path="address.thana"/>
            <form:select path="address.thana" cssClass="form-select" id="thanas" onchange="changeStreet();">
                <form:options items="${thanaList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.street" cssClass="form-label">Street</form:label>
            <form:errors path="address.street"/>
            <form:select path="address.street" cssClass="form-select" id="streets">
                <form:options items="${streetList}" itemLabel="name" itemValue="id"/>
            </form:select>
        </div>

        <div class="mb-3">
            <form:label path="address.latitude" cssClass="form-label">Latitude</form:label>
            <form:errors path="address.latitude"/>
            <form:input path="address.latitude" cssClass="form-control" id="latitude" onchange="changedLocation();"/>
        </div>

        <div class="mb-3">
            <form:label path="address.longitude" cssClass="form-label">Longitude</form:label>
            <form:errors path="address.longitude"/>
            <form:input path="address.longitude" cssClass="form-control" id="longitude" onchange="changedLocation();"/>
        </div>

        Select the location of the institution:<br>
        <div id="googleMap" style="width:100%;height:400px;"></div>

        <form:button type="submit" class="btn btn-primary">save</form:button>
    </form:form>
</div>

<script type="text/javascript">
    changeThana();

    function changeThana() {
        let districtId = document.getElementById("districts").value;
        let thanaElement = document.getElementById("thanas");
        thanaElement.options.length = 0;

        <c:forEach var="district" items="${districtList}">
        if (districtId == "${district.id}") {
            let index = 0;
            <c:forEach var="thana" items="${district.thanaList}">
            thanaElement.options[index] = new Option("${thana.name}", "${thana.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
        changeStreet();
    }

    function changeStreet() {
        let thanaId = document.getElementById("thanas").value;
        let streetElement = document.getElementById("streets");
        streetElement.options.length = 0;

        <c:forEach var="thana" items="${thanaList}">
        if (thanaId == "${thana.id}") {
            let index = 0;
            <c:forEach var="street" items="${thana.streetList}">
            streetElement.options[index] = new Option("${street.name}", "${street.id}", false, false);
            index++;
            </c:forEach>
        }
        </c:forEach>
    }

    let lati = 23.8096;
    let longi = 90.4085;

    <c:if test="${institution.address.latitude!=0.0}">
    lati = ${institution.address.latitude};
    longi = ${institution.address.longitude};
    </c:if>

    console.log("${institution.address.latitude}");

    document.getElementById("latitude").value = lati;
    document.getElementById("longitude").value = longi;

    function myMap() {
        let mapProp = {
            center: new google.maps.LatLng(lati, longi),
            zoom: 15,
        };
        let map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(lati, longi),
            map: map,
        });

        map.addListener("click", (mapsMouseEvent) => {
            marker.setPosition(mapsMouseEvent.latLng);
            lati = mapsMouseEvent.latLng.lat();
            longi = mapsMouseEvent.latLng.lng();
            document.getElementById("latitude").value = lati;
            document.getElementById("longitude").value = longi;
        });
    }

    function changedLocation() {
        lati = document.getElementById("latitude").value;
        longi = document.getElementById("longitude").value;
        myMap();
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYwjeAnf-0QpWYsvnG_fl7-5MtIcGvBRw&callback=myMap"></script>

</body>
</html>
