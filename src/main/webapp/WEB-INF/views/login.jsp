<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 29/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<c:url value="/login" var="loginLink"/>

<div class="p-5 rounded" style="background-color: #e3f2fd;">

    <p class="text-danger"><form:errors path="patient"/></p>
    <form:form modelAttribute="user" action="${loginLink}" method="post">
        <div class="mb-3">
            <form:label path="username" cssClass="form-label">Username</form:label>
            <form:errors path="username"/>
            <form:input path="username" cssClass="form-control" id="username"/>
        </div>
        <div class="mb-3">
            <form:label path="password" cssClass="form-label">Password</form:label>
            <form:errors path="password"/>
            <form:password path="password" cssClass="form-control" id="password"/>
        </div>
        <form:button type="submit" class="btn btn-primary">Login</form:button>
    </form:form>

</div>

<script>
    setActivePage();

    function setActivePage() {
        document.getElementById("login").classList.add("active");
    }
</script>

</body>
</html>
