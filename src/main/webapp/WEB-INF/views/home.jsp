<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 10/04/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>

</head>
<body>

<h5 class="text-center">Welcome to the Covid-19 Vaccination process</h5>

<div class="row">
    <div class="col-8">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner ">
                <div class="carousel-item active ">
                    <c:url value="/images/vac1.jpg" var="vaccinationImage1"/>
                    <img src="${vaccinationImage1}" class="d-block w-100" alt="AstraZeneca Vaccine">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>AstraZeneca Vaccine</h5>
                    </div>
                </div>
                <div class="carousel-item ">
                    <c:url value="/images/vac2.jpg" var="vaccinationImage2"/>
                    <img src="${vaccinationImage2}" class="d-block w-100" alt="A random person taking vaccine">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>A person taking vaccine</h5>
                    </div>
                </div>
                <div class="carousel-item ">
                    <c:url value="/images/vac3.jpg" var="vaccinationImage3"/>
                    <img src="${vaccinationImage3}" class="d-block w-100" alt="Zahid Malik, MP, taking vaccine">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Zahid Malik, minister of health, taking vaccine successfully</h5>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

    </div>
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <p class="card-text">For registration, click to below button.</p>
                <c:url value="/patient" var="registerLink">
                    <c:param name="id" value="0"/>
                </c:url>
                <a href="${registerLink}" class="btn btn-primary">Register</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <p class="card-text">Checkout Registration Stats.</p>
                <c:url value="/query" var="queryLink"></c:url>
                <a href="${queryLink}" class="btn btn-primary">Stats</a>
            </div>
        </div>
    </div>
</div>

<script>
    setActivePage();

    function setActivePage() {
        document.getElementById("home").classList.add("active");
    }
</script>

</body>
</html>
